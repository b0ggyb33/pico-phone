#include "pico/stdlib.h"

int8_t check_buttons();
void init_all_buttons();
void flash_output(uint8_t pin, uint8_t length);
