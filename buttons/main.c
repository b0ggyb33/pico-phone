#include "buttons.h"

int main(void)
{
  init_all_buttons();
  while (true)
  {
    int v = check_buttons();
    if (v >= 0)
    {
      for (int i=0; i<=v; i++)
      {
        flash_output(25, 250);
      }
    }
  }
  return 0;
}
