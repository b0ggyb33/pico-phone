#define PICO_LED 25
#define PHONE_LED 16 
#define PIN5 7
#define PIN6 8
#define PIN7 9
#define PIN8 10
#define PIN9 11
#define PIN10 12
#define PIN11 13
#define PIN12 14
#define PIN16 15


#define STAR 10
#define ZERO 11
#define HASH 12

#include "pico/stdlib.h"

void set_input(uint pin)
{
    gpio_init(pin);
    gpio_set_dir(pin, false);
    // pull the input low so we can check if its driven high by the switch
    gpio_pull_down(pin); 
}

void flash_output(uint pin, uint8_t length)
{
    gpio_put(pin, true);
    sleep_ms(length);
    gpio_put(pin, false);
    sleep_ms(length);
}

void set_output(uint pin)
{
    gpio_init(pin);
    gpio_set_dir(pin, /*out=*/true);
}

bool check_button(uint out, uint in)
{
    gpio_put(out, true);
    sleep_ms(5);
    bool pressed = gpio_get(in);
    sleep_ms(5);
    gpio_put(out, false);

    return pressed;
}

bool button_zero()
{
  return check_button(PIN8, PIN9);
}

bool button_one()
{
  return check_button(PIN6, PIN12);
}

bool button_two()
{
  return check_button(PIN7, PIN12);
}

bool button_three()
{
  return check_button(PIN16, PIN12);
}

bool button_four()
{
  return check_button(PIN5, PIN11);
}

bool button_five()
{
  return check_button(PIN7, PIN11);
}

bool button_six()
{
  return check_button(PIN16, PIN11);
}

bool button_seven()
{
  return check_button(PIN5, PIN10);
}

bool button_eight()
{
  return check_button(PIN7, PIN10);
}

bool button_nine()
{
  return check_button(PIN16, PIN10);
}

bool button_star()
{
  return check_button(PIN5, PIN9);
}

bool button_hash()
{
  return check_button(PIN16, PIN9);
}


int8_t check_buttons()
{
  if (button_zero()) return ZERO;
  if (button_one()) return 1;
  if (button_two()) return 2;
  if (button_three()) return 3;
  if (button_four()) return 4;
  if (button_five()) return 5;
  if (button_six()) return 6;
  if (button_seven()) return 7;
  if (button_eight()) return 8;
  if (button_nine()) return 9;
  if (button_star()) return STAR;
  if (button_hash()) return HASH;

  return -1;
}

void init_all_buttons()
{
    // ENABLE LED
    set_output(PICO_LED);
    set_output(PHONE_LED);
    
    set_output(PIN5);
    set_output(PIN6);
    set_output(PIN7);
    set_output(PIN8);
    set_output(PIN16);

    set_input(PIN9);
    set_input(PIN10);
    set_input(PIN11);
    set_input(PIN12);
}
