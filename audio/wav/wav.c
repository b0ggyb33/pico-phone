/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <math.h>

#if PICO_ON_DEVICE

#include "hardware/clocks.h"
#include "hardware/structs/clocks.h"

#endif

#include "hardware/flash.h"

#include "pico/stdlib.h"


#include "pico/audio_i2s.h"


#include "wav.h"
#include "buttons.h"

// flash definitions
#define FLASH_TARGET_OFFSET (512 * 1024)

//audio definitions
#define SAMPLES_PER_BUFFER 256

//keypad definitions
#define ENABLE_AMP_PIN 17


int16_t apply_volume(int16_t in);
void play_track(audio_buffer_pool_t* ap, uint8_t track);
void init_amp_toggle(void);
void turn_on_amp(void);
void turn_off_amp(void);


struct audio_buffer_pool *init_audio() {

    static audio_format_t audio_format = {
            .format = AUDIO_BUFFER_FORMAT_PCM_S16,
            .sample_freq = 16000, // 24000
            .channel_count = 1,
    };

    static struct audio_buffer_format producer_format = {
            .format = &audio_format,
            .sample_stride = 2
    };

    struct audio_buffer_pool *producer_pool = audio_new_producer_pool(&producer_format, 3,
                                                                      SAMPLES_PER_BUFFER); // todo correct size
    bool __unused ok;
    const struct audio_format *output_format;
    struct audio_i2s_config config = {
            .data_pin = PICO_AUDIO_I2S_DATA_PIN,
            .clock_pin_base = PICO_AUDIO_I2S_CLOCK_PIN_BASE,
            .dma_channel = 0,
            .pio_sm = 0,
    };

    output_format = audio_i2s_setup(&audio_format, &config);
    if (!output_format) {
        panic("PicoAudio: Unable to open audio device.\n");
    }

    ok = audio_i2s_connect(producer_pool);
    assert(ok);
    audio_i2s_set_enabled(true);
    return producer_pool;
}

int main() {

    stdio_init_all();

    init_all_buttons();

    init_amp_toggle();

    write_wavs();

    struct audio_buffer_pool *ap = init_audio();

    while (true) {
      
        int8_t val = check_buttons();

        if (val >=0 )
        {
          turn_on_amp();
          gpio_put(16, true);
          play_track(ap, val);
          gpio_put(16, false);
          turn_off_amp();
        }

    }
    return 0;
}

void play_track(audio_buffer_pool_t* ap, uint8_t track)
{
    const int16_t* flash_target_contents = (const int16_t*)(XIP_BASE+FLASH_TARGET_OFFSET);

    uint32_t pos  = get_track_start(track);
    uint32_t end = get_track_start(track+1);

    while(pos < end)
    {
      struct audio_buffer *buffer = take_audio_buffer(ap, true);

      int16_t *samples = (int16_t *) buffer->buffer->bytes;
      for (uint i = 0; i < buffer->max_sample_count; i++) {
          samples[i] = apply_volume(flash_target_contents[pos]);
          pos++;
      }
      buffer->sample_count = buffer->max_sample_count;

      give_audio_buffer(ap, buffer);
    }
}

int16_t apply_volume(int16_t in){
  return (128 * in) >> 8u;
}

void store_wav_in_flash(const uint32_t offset, const int16_t* sound_data, const uint32_t sound_length){

  erase_flash(offset, sound_length);
  write_flash(offset, sound_data, sound_length);

}

void write_flash(const uint32_t offset, const int16_t* sound_data, const uint32_t sound_length)
{
  flash_range_program(FLASH_TARGET_OFFSET + (offset * 2), (const uint8_t*) sound_data, sound_length*2); // must be a multiple of 256 bytes;
}

void erase_flash(const uint32_t offset, const uint32_t sound_length)
{
  uint32_t number_of_bytes_to_erase = sound_length * 2; // snd_wav_is_16bit
  uint32_t sectors_to_erase = (number_of_bytes_to_erase / FLASH_SECTOR_SIZE) + 1;
  flash_range_erase(FLASH_TARGET_OFFSET + (offset * 2), sectors_to_erase * FLASH_SECTOR_SIZE); // sector size is 4096 bytes
}

void write_a_flash_page(const uint32_t flash_base_offset, const int16_t* sound_data, uint32_t sound_length)
{
   flash_range_program(FLASH_TARGET_OFFSET + flash_base_offset, (const uint8_t*) sound_data, FLASH_PAGE_SIZE); // must be a multiple of 256 bytes;
}


void init_amp_toggle(void)
{
  gpio_init(ENABLE_AMP_PIN);
  gpio_set_dir(ENABLE_AMP_PIN, true);
  turn_off_amp();
}

void turn_on_amp(void)
{
  gpio_put(ENABLE_AMP_PIN, true);
}

void turn_off_amp(void)
{
  gpio_put(ENABLE_AMP_PIN, false);
}
