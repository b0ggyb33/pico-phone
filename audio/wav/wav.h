#include "pico/stdlib.h"

uint32_t write_wavs(void);
void store_wav_in_flash(const uint32_t flash_base_offset, const int16_t* sound_data, const uint32_t sound_length);
void erase_flash(const uint32_t flash_base_offset, uint32_t sound_length);
void write_flash(const uint32_t flash_base_offset, const int16_t* sound_data, const uint32_t sound_length);
void write_a_flash_page(const uint32_t flash_base_offset, const int16_t* sound_data, uint32_t sound_length);

uint32_t get_track_start(uint8_t track);
