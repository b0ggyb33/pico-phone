from machine import Pin, Timer

PIN5 = 7
PIN6 = 8
PIN7 = 9
PIN8 = 10
PIN9 = 11
PIN10 = 12
PIN11 = 13
PIN12 = 14
PIN13 = 0
PIN16 = 15

phone_led = Pin(PIN13, Pin.OUT)
phone_led.low()
t = Timer()

def tick(timer):
    led.toggle()
    
def zero():
    is_button_checked(0, PIN8, PIN9)

def one():
    is_button_checked(1, PIN6, PIN12)    

def two():
    is_button_checked(2, PIN7 , PIN12)
        
def five():
    is_button_checked(5, PIN7, PIN11)

def eight():
    is_button_checked(8, PIN7, PIN10)
    
def four():
    is_button_checked(4, PIN5, PIN11)

def seven():
    is_button_checked(7, PIN5, PIN10)

def starkey():
    is_button_checked('*', PIN5, PIN9)
    
def three():
    is_button_checked(3, PIN16 , PIN12)    
    
def six():
    is_button_checked(6, PIN16, PIN11)
    
def nine():
    is_button_checked(9, PIN16, PIN10)

def hashkey():
    is_button_checked('#', PIN16, PIN9)
    
def is_button_checked(value, inpin, outpin):
    pinout = Pin(outpin, Pin.OUT)
    pinout.high()
    pinin = Pin(inpin, Pin.IN, Pin.PULL_DOWN)
    if pinin.value():
        print(value)
    pinout.low()

    
def button_checks(timer):
    zero()
    one()
    two()
    three()
    four()
    five()
    six()
    seven()
    eight()
    nine()
    starkey()
    hashkey()
    
    
t.init(freq=10, mode=Timer.PERIODIC, callback=button_checks)
